import React from 'react'
import { StoreProvider } from 'easy-peasy'
import { BrowserRouter } from 'react-router-dom'
import { ApolloProvider } from '@apollo/client'

import { store, apolloClient } from './core'
import { AppRoutes } from './AppRoutes'
import { TokenValidator } from './components'

import './app.css'

export const App = () => (
  <ApolloProvider client={apolloClient()}>
    <BrowserRouter>
      <StoreProvider store={store}>
        <TokenValidator />
        <AppRoutes />
      </StoreProvider>
    </BrowserRouter>
  </ApolloProvider>
)
