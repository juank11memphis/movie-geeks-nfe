import { getToken, getResetPasswordToken, removeToken, setTestIsLoggedIn } from './localStorage'

export const getBearerToken = () => {
  const token = getToken()
  return token ? `bearer ${token}` : null
}

export const getBearerResetPasswordToken = () => {
  const token = getResetPasswordToken()
  return token ? `bearer ${token}` : null
}

export const signOut = () => {
  removeToken()
  setTestIsLoggedIn(false)
  window.location.href = '/'
}
