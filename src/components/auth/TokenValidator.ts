import React from 'react'
import { useApolloClient } from '@apollo/client'
import { useStoreActions } from 'easy-peasy'

import { localStorageUtil } from '../../shared'
import { authQueries } from '../../shared'

export const TokenValidator = React.memo(() => {
  const setCurrentUser = useStoreActions((dispatch: any) => dispatch.currentUser.setUser)
  const apolloClient = useApolloClient()
  const validateToken = async () => {
    try {
      const { data } = await apolloClient.mutate({
        mutation: authQueries.REFRESH_TOKEN,
      })
      const { auth } = data
      const { user } = auth
      setCurrentUser(user)
    } catch (error) {
      localStorageUtil.removeToken()
      setCurrentUser(null)
    }
  }
  validateToken()
  return null
})
