import { createStore } from 'easy-peasy'

import { currentUser } from './currentUser'
import { search } from './search'
import { profile } from './profile'

export const store = createStore({
  currentUser,
  search,
  profile,
})
