import { action } from 'easy-peasy'

export const profile = {
  subRoute: null,
  setSubRoute: action((state, subRoute) => ({
    ...state,
    subRoute,
  })),
  filters: null,
  setFilters: action((state, filters) => ({
    ...state,
    filters,
    filtersDirty: true,
  })),
  filtersDirty: false,
  setFiltersDirty: action((state, filtersDirty) => ({
    ...state,
    filtersDirty,
  })),
}
