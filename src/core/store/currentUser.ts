import { action } from 'easy-peasy'

export const currentUser = {
  user: null,
  fetched: false,
  setUser: action((state, user) => ({
    ...state,
    user,
    fetched: true,
  })),
}
