import { action } from 'easy-peasy'

export const search = {
  query: null,
  setQuery: action((state, query) => ({
    ...state,
    query,
  })),
}
