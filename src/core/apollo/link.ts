// import { setContext } from 'apollo-link-context'
import { ApolloLink, createHttpLink } from '@apollo/client'
import { setContext } from '@apollo/link-context'

import { authUtil, localStorageUtil } from '../../shared'

const httpLink = createHttpLink({ uri: 'http://localhost:3000/graphql' })

const getTestHeaders = () => ({
  'x-test-response-mode': localStorageUtil.getTestResponseMode(),
  'x-test-logged-in': localStorageUtil.getTestIsLoggedIn(),
  'x-test-service-data-override': localStorageUtil.getTestServiceDataOverride(),
})

const contextLink = setContext(() => {
  let testHeaders = {}
  if (process.env.NODE_ENV === 'test') {
    testHeaders = getTestHeaders()
  }
  return {
    headers: {
      authorization: authUtil.getBearerToken(),
      'reset-password-token': authUtil.getBearerResetPasswordToken(),
      ...testHeaders,
    },
  }
})

const handleAuthTokenLink = new ApolloLink((operation, forward) =>
  forward(operation).map(response => {
    const { data } = response
    const { auth } = data as any
    if (auth && auth.token) {
      localStorageUtil.storeToken(auth.token)
    }
    return response
  }),
)

export const link = ApolloLink.from([contextLink, handleAuthTokenLink, httpLink])
