import React from 'react'
import { Route, Switch } from 'react-router-dom'

// import {
//   HomePage,
//   SignInPage,
//   ForgotPasswordPage,
//   SignUpPage,
//   ResetPasswordPage,
//   SearchPage,
//   ProfilePage,
//   DiscoverPage,
// } from './views'

export const AppRoutes = () => (
  <Switch>
    <Route exact path="/" render={() => <div>Home Page</div>} />
    <Route exact path="/signup" render={() => <div>Signup Page</div>} />
    <Route exact path="/signin" render={() => <div>Signin Page</div>} />
    <Route exact path="/forgot-password" render={() => <div>Forgot Password Page</div>} />
    <Route exact path="/reset-password" render={() => <div>Reset Password Page</div>} />
    <Route exact path="/search" render={() => <div>Search Page</div>} />
    <Route
      path="/profile/:id"
      render={({
        match: {
          params: { id },
        },
      }) => <div>Profile Page</div>}
    />
    <Route exact path="/discover" render={() => <div>Discover Page</div>} />
  </Switch>
)
